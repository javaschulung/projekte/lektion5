package de.ruv.javaschulung.Lektion5SchniKoRek.Gui;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;

public class MenuButton extends BlueButton {

	public MenuButton(String text) {
		super(text);
		this.setMaximumSize(new Dimension(Integer.MAX_VALUE, this.getMinimumSize().height));
	}
	
}
