package de.ruv.javaschulung.Lektion5SchniKoRek.Gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainWindow extends JFrame {
	
	final Dimension DIMENSION = new Dimension(800, 600);
	public static final String DRAWABLE_PANEL = "DrawablePanel";
	public static final String FIB_PANEL = "FibPanel";
	
	private LeftPanel leftPanel;
	private HashMap<String, JPanel> panels = new HashMap<>();
	private JPanel contentPanel;
	
	public MainWindow() {
		super("Schnittstellen, Konstanten & Rekursion");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setMinimumSize(DIMENSION);
		this.setSize(DIMENSION);
		
		this.leftPanel = new LeftPanel(this);
		this.add(this.leftPanel, BorderLayout.WEST);
		
		panels.put(DRAWABLE_PANEL, new DrawablePanel(this));
		panels.put(FIB_PANEL, new FibPanel(this));
		
		showPanel("DrawablePanel");
		
		//this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public void showPanel(String name) {
		this.setContentPanel(this.panels.get(name));
	}
	
	public void setContentPanel(JPanel panel) {
		if (contentPanel != null) {
			this.remove(contentPanel);
		}
		
		this.contentPanel = panel;
		this.add(panel, BorderLayout.CENTER);
		this.repaint();
		this.revalidate();
	}
	
}
