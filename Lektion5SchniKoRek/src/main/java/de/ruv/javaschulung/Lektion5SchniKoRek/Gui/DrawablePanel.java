package de.ruv.javaschulung.Lektion5SchniKoRek.Gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Optional;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Circle;
import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Oval;
import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Rectangle;
import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Square;
import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Vector;
import de.ruv.javaschulung.Lektion5SchniKoRek.Logic.Drawer;

public class DrawablePanel extends CenterPanel {

	JPanel topPanel;
	JPanel canvas;
	Drawer drawer;
	
	public DrawablePanel(MainWindow p) {
		super(p);		
		this.setLayout(new BorderLayout());
		
		this.canvas = new JPanel();
		this.canvas.setBackground(Color.WHITE);
		this.add(canvas, BorderLayout.CENTER);
		
		this.drawer = new Drawer( () -> Optional.ofNullable(canvas.getGraphics()) );
		
		this.topPanel = new JPanel();
		this.topPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 60, 2));
		//topPanel.setBackground(Color.WHITE);
		this.topPanel.setBackground(new Color(0, 153, 204));
		this.topPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.LIGHT_GRAY));
		this.add(this.topPanel, BorderLayout.NORTH);
		
		JButton rectangle = new BlueButton("Rechteck");
		rectangle.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				int posX = canvas.getWidth()/2 - 50;
				int posY = canvas.getHeight()/2 - 100;
				drawer.setDrawable(new Rectangle(new Vector(posX,posY), 100, 200, new Vector(0,0)));
				drawer.draw();
			}
		});
		this.topPanel.add(rectangle);
		
		JButton square = new BlueButton("Quadrat");
		square.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				int posX = canvas.getWidth()/2 - 100;
				int posY = canvas.getHeight()/2 - 100;
				drawer.setDrawable(new Square(new Vector(posX,posY), 200, new Vector(0,0)));
				drawer.draw();
			}
		});
		this.topPanel.add(square);
		
		JButton oval = new BlueButton("Oval");
		oval.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				int posX = canvas.getWidth()/2 - 50;
				int posY = canvas.getHeight()/2 - 100;
				drawer.setDrawable(new Oval(new Vector(posX,posY), 100, 200, new Vector(0,0)));
				drawer.draw();
			}
		});
		this.topPanel.add(oval);
		
		JButton circle = new BlueButton("Kreis");
		circle.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				int posX = canvas.getWidth()/2 - 100;
				int posY = canvas.getHeight()/2 - 100;
				drawer.setDrawable(new Circle(new Vector(posX,posY), 200, new Vector(0,0)));
				drawer.draw();
			}
		});
		this.topPanel.add(circle);
	}

}
