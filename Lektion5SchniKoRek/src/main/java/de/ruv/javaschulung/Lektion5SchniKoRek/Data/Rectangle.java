package de.ruv.javaschulung.Lektion5SchniKoRek.Data;

public class Rectangle extends GeometricObject {

	public Rectangle(Vector pos, double width, double height, Vector mov) {
		super(pos, width, height, mov);
	}

	@Override
	public double area() {
		return this.getHeight() * this.getWidth();
	}

	@Override
	public boolean equals(Object obj) {
		
		if ( !super.equals(obj) ) {
			return false;
		}
		
		if ( !(obj instanceof Rectangle) ) {
			return false;
		}
		
		return true;
	}
}
