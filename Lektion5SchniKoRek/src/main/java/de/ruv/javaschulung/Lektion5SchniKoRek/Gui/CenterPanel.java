package de.ruv.javaschulung.Lektion5SchniKoRek.Gui;

import java.awt.Color;

import javax.swing.JPanel;

public class CenterPanel extends JPanel {

	MainWindow parent;
	
	public CenterPanel(MainWindow p) {
		this.parent = p;
		
		this.setBackground(Color.WHITE);
	}
	
}
