package de.ruv.javaschulung.Lektion5SchniKoRek.Gui;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class TopLabel extends JLabel {

	public TopLabel(String text) {
		super(text);
		
		this.setMaximumSize(new Dimension(Integer.MAX_VALUE, this.getMinimumSize().height*2));
		this.setVerticalAlignment(SwingConstants.CENTER);
		this.setHorizontalAlignment(SwingConstants.CENTER);
		this.setOpaque(true);
		//this.setBackground(Color.BLUE);
		this.setBackground(new Color(0, 51, 153));
		this.setForeground(Color.WHITE);
	}
	
}
