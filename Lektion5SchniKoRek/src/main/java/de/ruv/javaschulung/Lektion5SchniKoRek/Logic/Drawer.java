package de.ruv.javaschulung.Lektion5SchniKoRek.Logic;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Optional;
import java.util.function.Supplier;

import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Drawable;

public class Drawer {

	Supplier<Optional<Graphics>> supplier;
	Drawable drawable;
		
	public Drawer(Supplier<Optional<Graphics>> supplier) {
		this.supplier = supplier;
	}
	
	public void draw() {		
		supplier.get().ifPresent((g) -> {
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, Integer.MAX_VALUE, Integer.MAX_VALUE);
			g.setColor(Color.BLACK);
			drawable.draw(g);
		});		
	}
	
	public void setDrawable(Drawable drawable) {
		this.drawable = drawable;
	}
}
