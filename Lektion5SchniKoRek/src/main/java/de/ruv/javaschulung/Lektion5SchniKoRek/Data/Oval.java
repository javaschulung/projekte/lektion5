package de.ruv.javaschulung.Lektion5SchniKoRek.Data;

public class Oval extends GeometricObject {

	public Oval(Vector pos, double width, double height, Vector mov) {
		super(pos, width, height, mov);
	}

	@Override
	public double area() {
		return this.getHeight() * this.getWidth() * Math.PI;
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( !super.equals(obj) ) {
			return false;
		}
		
		if ( !(obj instanceof Oval) ) {
			return false;
		}
		
		return true;
	}

}
