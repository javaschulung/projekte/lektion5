package de.ruv.javaschulung.Lektion5SchniKoRek.Gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class LeftPanel extends JPanel {

	MainWindow parent;
	
	public LeftPanel(MainWindow p) {
		this.parent = p;
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setBackground(Color.LIGHT_GRAY);
		//this.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
		this.setBorder(null);
		
		JLabel label = new TopLabel("Menü");		
		this.add(label);
		
		this.add(Box.createRigidArea(new Dimension(0, 10)));
		
		JButton interfaces = new MenuButton("Schnittstellen");
		interfaces.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				//parent.setContentPanel(new DrawablePanel(parent));
				parent.showPanel(MainWindow.DRAWABLE_PANEL);
			}
		});
		this.add(interfaces);
		
		this.add(Box.createRigidArea(new Dimension(0, 5)));
		
		JButton recursion = new MenuButton("Rekursion");
		recursion.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				//parent.setContentPanel(new FibPanel(parent));
				parent.showPanel(MainWindow.FIB_PANEL);
			}
		});
		this.add(recursion);
		
	}
	
}
