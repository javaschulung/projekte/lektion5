package de.ruv.javaschulung.Lektion5SchniKoRek.Data;

public class Vector {

	private double x;
	private double y;
	
	public Vector(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if ( !(obj instanceof Vector) ) {
			return false;
		}
		
		Vector that = (Vector) obj;
		
		if (this.x != that.x) {
			return false;
		}
		
		if (this.y != that.y) {
			return false;
		}
		
		return true;
	}
}
