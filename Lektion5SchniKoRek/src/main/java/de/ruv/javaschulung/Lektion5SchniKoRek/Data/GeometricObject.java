package de.ruv.javaschulung.Lektion5SchniKoRek.Data;

public abstract class GeometricObject {

	private Vector position;
	private Vector movement;
	private double width;
	private double height;
	
	public GeometricObject(Vector pos, double width, double height, Vector mov) {
		this.position = pos;
		this.width = width;
		this.height = height;
		this.movement = mov;
	}
	
	public Vector getPosition() {
		return this.position;
	}
	
	public Vector getMovement() {
		return this.movement;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	public double getHeight() {
		return this.height;
	}
	
	public void move(Vector v) {
		double newX = this.position.getX() + v.getX();
		double newY = this.position.getY() + v.getY();
		this.position = new Vector(newX, newY);
	}
	
	public void move() {
		move(this.movement);
	}
	
	public void moveTo(Vector v) {
		this.position = v;
	}
	
	public abstract double area();
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if ( !(obj instanceof GeometricObject) ) {
			return false;
		}
		
		GeometricObject that = (GeometricObject) obj;
		
		if (this.width != that.width) {
			return false;
		}
		
		if (this.height != that.height) {
			return false;
		}
		
		if ( !this.position.equals(that.position) ) {
			return false;
		}
		
		if ( !this.movement.equals(that.movement) ) {
			return false;
		}
				
		return true;
	}
	
}
