package de.ruv.javaschulung.Lektion5SchniKoRek.Gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import de.ruv.javaschulung.Lektion5SchniKoRek.Logic.Fibonacci;

public class FibPanel extends CenterPanel {

	public FibPanel(MainWindow p) {
		super(p);
		
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		
		JLabel label = new JLabel("Berechne die n-te Fibonacci-Zahl:");
		this.add(label);
		layout.putConstraint(SpringLayout.WEST, label, 20, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, label, 20, SpringLayout.NORTH, this);
		
		JTextField tf = new JTextField("1", 15);
		tf.setHorizontalAlignment(JTextField.RIGHT);
		this.add(tf);
		layout.putConstraint(SpringLayout.WEST, tf, 20, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, tf, 10, SpringLayout.SOUTH, label);
		
		JButton btn = new BlueButton("Berechnen");
		this.add(btn);
		layout.putConstraint(SpringLayout.WEST, btn, 10, SpringLayout.EAST, tf);
		layout.putConstraint(SpringLayout.NORTH, btn, 10, SpringLayout.SOUTH, label);
		tf.setPreferredSize(btn.getPreferredSize());
		//tf.setMinimumSize(new Dimension(tf.getMinimumSize().width, 100));
		//tf.setSize(new Dimension(tf.getMinimumSize().width, 100));
		
		JTextArea ta = new JTextArea();
		JScrollPane sp = new JScrollPane(ta);
		this.add(sp);
		layout.putConstraint(SpringLayout.WEST, sp, 20, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, sp, 10, SpringLayout.SOUTH, tf);
		layout.putConstraint(SpringLayout.SOUTH, sp, -20, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.EAST, sp, -20, SpringLayout.EAST, this);
		
		btn.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int n = 0;
				try {
					n = Integer.parseInt(tf.getText());
				} catch (Exception e) {
					ta.append("Ungültige Eingabe!\n");
					return;
				}
				
				long fib = Fibonacci.fib(n);
				
				String s = new StringBuilder()
						.append("Die ")
						.append(n)
						.append("-te Finbonacci-Zahl ist: ")
						.append(fib)
						.append("\n")
						.toString();
				
				ta.append(s);
			}
		});
	}

}
