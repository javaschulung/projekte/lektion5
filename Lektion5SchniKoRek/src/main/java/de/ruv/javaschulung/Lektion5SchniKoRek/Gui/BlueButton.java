package de.ruv.javaschulung.Lektion5SchniKoRek.Gui;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;

public class BlueButton extends JButton {

	public BlueButton(String text) {
		super(text);
		this.setBackground(new Color(102, 204, 255));
		this.setBorderPainted(false);
		this.setFocusPainted(false);
		this.setForeground(Color.WHITE);
	}
		
}
