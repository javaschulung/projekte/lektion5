package de.ruv.javaschulung.Lektion5SchniKoRek;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Vector;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class FinalTest extends TestCase {

	public FinalTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(FinalTest.class);
	}

	public void finalVectorFieldExists(String fieldName, double x, double y) {
		
		Class<Vector> vector = Vector.class;
		
		try {
			Field field = vector.getField(fieldName);
			assertNotNull(field);
			
			assertEquals("Datentyp", Vector.class, field.getType());
			
			assertTrue("Feld ist nicht final", Modifier.isFinal(field.getModifiers()));
			
			Vector v = (Vector)field.get(null);
			
			assertNotNull(v);
			
			assertEquals(x, v.getX());
			assertEquals(y, v.getY());
			
		} catch (Exception e) {
			fail("Feld " + fieldName + " fehlt oder wurde falsch implementiert!");
		}
		
	}
	
	public void testFinalVectorFields() {
		Vector vector = new Vector(0, 0);
		Class<? extends Vector> arrayClass = vector.getClass();

		try {
			Field field = arrayClass.getDeclaredField("x");
			assertNotNull(field);

			assertEquals("Datentyp", double.class, field.getType());

			assertTrue("Feld ist nicht final", Modifier.isFinal(field.getModifiers()));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Feld x fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Field field = arrayClass.getDeclaredField("y");
			assertNotNull(field);

			assertEquals("Datentyp", double.class, field.getType());

			assertTrue("Feld ist nicht final", Modifier.isFinal(field.getModifiers()));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Feld y fehlt oder wurde falsch implementiert!");
		}
	}
	
	public void testFinalVectorExists() {
		
		finalVectorFieldExists("ZERO",  0.0,  0.0);
		finalVectorFieldExists("UP",    0.0, -1.0);
		finalVectorFieldExists("DOWN",  0.0,  1.0);
		finalVectorFieldExists("LEFT", -1.0,  0.0);
		finalVectorFieldExists("RIGHT", 1.0,  0.0);
		
	}
	
}
