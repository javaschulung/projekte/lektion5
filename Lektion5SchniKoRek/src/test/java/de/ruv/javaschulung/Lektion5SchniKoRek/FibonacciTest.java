package de.ruv.javaschulung.Lektion5SchniKoRek;

import de.ruv.javaschulung.Lektion5SchniKoRek.Logic.Fibonacci;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class FibonacciTest extends TestCase {

	public FibonacciTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(FibonacciTest.class);
	}

	// https://de.wikipedia.org/wiki/Fibonacci-Folge#Formel_von_Moivre-Binet
	private long explicitFib(int n) {
		return (long)(1 / Math.sqrt(5) * (  Math.pow( (1 + Math.sqrt(5)) / 2, n) - Math.pow( (1 - Math.sqrt(5)) / 2 , n)  ));
	}
	
	public void testFibonacci() {
		for (int i = 0; i < 40; i++) {
			assertEquals(explicitFib(i), Fibonacci.fib(i));
		}
	}
}
