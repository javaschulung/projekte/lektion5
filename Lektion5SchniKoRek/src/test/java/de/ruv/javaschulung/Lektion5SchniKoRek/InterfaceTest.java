package de.ruv.javaschulung.Lektion5SchniKoRek;

import java.awt.Graphics;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Circle;
import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Drawable;
import de.ruv.javaschulung.Lektion5SchniKoRek.Data.GeometricObject;
import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Oval;
import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Rectangle;
import de.ruv.javaschulung.Lektion5SchniKoRek.Data.Square;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class InterfaceTest extends TestCase {

	public InterfaceTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(InterfaceTest.class);
	}

	public void testInterface() {
		Class<Drawable> drawable = Drawable.class;
		
		assertTrue(drawable.isInterface());
		
		try {
			Method meth = drawable.getMethod("draw", new Class[] {Graphics.class});
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", void.class, meth.getReturnType());
			
			assertTrue("Methode ist nicht abstrakt", Modifier.isAbstract(meth.getModifiers()));
			
		} catch (Exception e) {
			fail("Methode draw(Graphics) fehlt oder wurde falsch implementiert!");
		}
		
		
		Class<GeometricObject> geoClass = GeometricObject.class;
		
		assertTrue("Klasse ist nicht abstrakt", Modifier.isAbstract(geoClass.getModifiers()));
		
		try {
			Method meth = geoClass.getMethod("draw", new Class[] {Graphics.class});
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", void.class, meth.getReturnType());
			
			assertTrue("Methode ist nicht abstrakt", Modifier.isAbstract(meth.getModifiers()));
			
		} catch (Exception e) {
			fail("Methode draw(Graphics) fehlt oder wurde falsch implementiert!");
		}
	}
	
	public void testImplements() {
		// Rectangle
		Class<Rectangle> rectangle = Rectangle.class;
		
		assertFalse("Klasse ist abstrakt", Modifier.isAbstract(rectangle.getModifiers()));
		
		try {
			Method meth = rectangle.getMethod("draw", new Class[] {Graphics.class});
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", void.class, meth.getReturnType());
			
			assertFalse("Methode ist abstrakt", Modifier.isAbstract(meth.getModifiers()));
			
		} catch (Exception e) {
			fail("Methode draw(Graphics) fehlt oder wurde falsch implementiert!");
		}
		
		// Square
		Class<Square> square = Square.class;
		
		assertFalse("Klasse ist abstrakt", Modifier.isAbstract(square.getModifiers()));
		
		try {
			Method meth = square.getMethod("draw", new Class[] {Graphics.class});
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", void.class, meth.getReturnType());
			
			assertFalse("Methode ist abstrakt", Modifier.isAbstract(meth.getModifiers()));
			
		} catch (Exception e) {
			fail("Methode draw(Graphics) fehlt oder wurde falsch implementiert!");
		}
		
		// Rectangle
		Class<Oval> oval = Oval.class;
		
		assertFalse("Klasse ist abstrakt", Modifier.isAbstract(oval.getModifiers()));
		
		try {
			Method meth = oval.getMethod("draw", new Class[] {Graphics.class});
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", void.class, meth.getReturnType());
			
			assertFalse("Methode ist abstrakt", Modifier.isAbstract(meth.getModifiers()));
			
		} catch (Exception e) {
			fail("Methode draw(Graphics) fehlt oder wurde falsch implementiert!");
		}
		
		// Rectangle
		Class<Circle> circle = Circle.class;
		
		assertFalse("Klasse ist abstrakt", Modifier.isAbstract(circle.getModifiers()));
		
		try {
			Method meth = circle.getMethod("draw", new Class[] {Graphics.class});
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", void.class, meth.getReturnType());
			
			assertFalse("Methode ist abstrakt", Modifier.isAbstract(meth.getModifiers()));
			
		} catch (Exception e) {
			fail("Methode draw(Graphics) fehlt oder wurde falsch implementiert!");
		}
	}
}
